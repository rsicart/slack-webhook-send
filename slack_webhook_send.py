from slack_sdk.webhook import WebhookClient
import os
import sys

url = os.environ.get('SLACK_WEBHOOK_URL')
if url is None:
    raise ValueError("Webhook url is mandatory. Define environment var SLACK_WEBHOOK_URL.")

if len(sys.argv) != 2:
    raise ValueError("Message is mandatory. Call this script with a positional argument.")
msg = sys.argv[1]

# https://slack.dev/python-slack-sdk/webhook/index.html
webhook = WebhookClient(url)
response = webhook.send(
    text=msg,
    blocks=[
        {
            "type": "section",
            "text": {
                "type": "mrkdwn",
                "text": msg
            }
        }
    ]
)


assert response.status_code == 200
assert response.body == "ok"
