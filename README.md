# slack webhook send

Script to send messages to a Slack channel, using Webhook client.

Useful to use in pipelines.

## Install dependencies

* python3
* pip3
* python modules

```
pip3 install -r requirements.txt
```

## Examples

Launch from cli:

```
export SLACK_WEBHOOK_URL="https://hooks.slack.com/services/T00000000/B00000000/XXXXXXXXXXXXXXXXXXXXXXXX"
python3 scripts/slack_webhook_send.py "Title\n\nMesssage body"
```


Use it from gitlab-ci.

Send [kubent](https://github.com/doitintl/kube-no-trouble) output to slack:

```
.kubent: &kubent
  stage: apply tools
  <<: *before_script
  script:
    - pip3 install slack_sdk
    - sh -c "$(curl -sSL https://git.io/install-kubent)"
    - |-
        if ! OUTPUT="$(kubent)"; then       # check for non-zero return code first
          echo "kubent failed to run!"
        elif [ -n "${OUTPUT}" ]; then       # check for empty stdout
          echo "Deprecated resources found!"
          echo "${OUTPUT}"
          python3 scripts/slack_webhook_send.py "*kubent - ${CI_JOB_NAME}*
           :warning: Deprecated resources found!
           Please check full output on <${CI_JOB_URL}|pipeline job>."
        fi
  only:
    - master

Kubent Report:
  variables:
    SLACK_WEBHOOK_URL: "https://hooks.slack.com/services/T00000000/B00000000/XXXXXXXXXXXXXXXXXXXXXXXX"
  tags:
    - kubernetes
  <<: *kubent
```
